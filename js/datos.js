

var servicios = $.getJSON("data/servicios.json");
var personas  = $.getJSON("data/personas.json");
var detalles  = $.getJSON("data/detalle.json");

function mostrar() {
	$('#datos > tfoot').show();
	$('#menos').show();
	$('#mas').hide();
}

function ocultar() {
	$('#menos').hide();
	$('#mas').show();
	$('#datos > tfoot').hide();
}

function cargarDatos() {

	$('#datos > tfoot').hide();
	$('#datos > thead > tr').remove();
	$('#datos > tbody > tr').remove();
	$('#datos > tfoot > tr').remove();
	var servicio = Array();
	var totalS = Array();
	var totalD = Array();

	for (x = 0; x < servicios.responseJSON.length; x++){
		sr = servicios.responseJSON[x].nombreservicio;
		servicio[x] = sr;
	}

	totalSer = 0; 
	for (x = 0; x < servicios.responseJSON.length; x++){
		suma = 0; 
		for (y = 0; y < detalles.responseJSON.length; y++) {
			if (servicios.responseJSON[x].nombreservicio == detalles.responseJSON[y].nombreservicio) {
				mnt = detalles.responseJSON[y].monto;
				suma += parseFloat(mnt);
			}
		}
		totalS[x] = suma;
		totalSer += suma; 
	}

    $('#datos > thead').append('<tr><th width="10%"></th>');

    for (x = 0; x < servicio.length; x++) {
    	sr = servicios.responseJSON[x].nombreservicio;
		servicio[x] = sr;
		$('#datos > thead > tr').append('<th class="c">' + servicio[x] + '</th>');
	}

	$('#datos > thead > tr').append('<th class="c">TOTALES</th>');

	$('#datos > thead').append('</tr>');

	$('#datos > tbody').append('<tr><td class="c">' +
		'<div id="mas" onclick="mostrar()" class="btn-mm">' + 
		'<span class="glyphicon glyphicon-plus-sign verde"></span></div>' +
		'<div id="menos" onclick="ocultar()" style="display:none" class="btn-mm">' +
		'<span class="glyphicon glyphicon-minus-sign rojo"></span></div></td>');	

	for (y = 0; y < totalS.length; y++){
		$('#datos > tbody > tr').append('<td class="c">' + totalS[y] + '</td>');
	}

	$('#datos > tbody > tr').append('<td class="c n">' + totalSer +'</td>');

	$('#datos > tbody').append('</tr>');

	totalD = 0;
	for (x = 0; x < personas.responseJSON.length; x++){
		totalxP = 0;
		$('#datos > tfoot').append('<tr><td class="c n">' + personas.responseJSON[x].nombrepersona +'</td>');
		for (y = 0; y < servicios.responseJSON.length; y++) {
			sum = 0; 
			for (z = 0; z < detalles.responseJSON.length; z++) {
				if ((servicios.responseJSON[y].nombreservicio == detalles.responseJSON[z].nombreservicio) && (personas.responseJSON[x].nombrepersona == detalles.responseJSON[z].nombrepersona)) {
					sum += detalles.responseJSON[z].monto;		
				}
			}
			totalD = sum;
			totalxP += totalD;
			$('#datos > tfoot > tr:nth-child('+(x+1)+')').append('<td class="c">' + totalD + '</td>');	
		}

		$('#datos > tfoot > tr:nth-child('+(x+1)+')').append('<td class="c n">' + totalxP +'</td></tr>');
		$('#datos > tfoot').append('</tr>');	
	}
}